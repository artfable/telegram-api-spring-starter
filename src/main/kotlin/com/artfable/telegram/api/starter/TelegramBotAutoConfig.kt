package com.artfable.telegram.api.starter

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

/**
 * @author aveselov
 * @since 10/02/2021
 */
@Configuration
@ComponentScan
class TelegramBotAutoConfig {

}