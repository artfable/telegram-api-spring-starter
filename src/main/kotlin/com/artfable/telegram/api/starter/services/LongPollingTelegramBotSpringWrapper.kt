package com.artfable.telegram.api.starter.services

import com.artfable.telegram.api.Behaviour
import com.artfable.telegram.api.CallbackBehaviour
import com.artfable.telegram.api.LongPollingTelegramBot
import com.artfable.telegram.api.service.TelegramSender
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.task.TaskExecutor
import org.springframework.stereotype.Service
import javax.annotation.PostConstruct

/**
 * @author aveselov
 * @since 10/02/2021
 */
@Service
abstract class LongPollingTelegramBotSpringWrapper(
    private val behaviours: Set<Behaviour>,
    private val callbackBehaviours: Set<CallbackBehaviour> = setOf(),
    private val skipFailed: Boolean = true
) {
    @Autowired
    private lateinit var taskExecutor: TaskExecutor

    @Autowired
    private lateinit var telegramSender: TelegramSender

    private lateinit var longPollingTelegramBot: LongPollingTelegramBot

    /**
     * For compatibility with java
     */
    constructor(behaviours: Set<Behaviour>, callbackBehaviours: Set<CallbackBehaviour>) : this(
        behaviours,
        callbackBehaviours,
        true
    )

    @PostConstruct
    private fun init() {
        longPollingTelegramBot =
            LongPollingTelegramBot(taskExecutor, telegramSender, behaviours, callbackBehaviours, skipFailed)
        longPollingTelegramBot.init()
    }
}