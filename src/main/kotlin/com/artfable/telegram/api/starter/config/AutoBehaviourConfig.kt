package com.artfable.telegram.api.starter.config

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.io.Resource
import com.artfable.telegram.api.service.SetCommandsSetup
import com.artfable.telegram.api.service.TelegramSender

/**
 * @author aveselov
 * @since 10/02/2021
 */
@Configuration
class AutoBehaviourConfig {

    @Bean
    @ConditionalOnProperty("telegram.bot.commands")
    fun setCommandsSetup(@Value("\${telegram.bot.commands}") resource: Resource, objectMapper: ObjectMapper, telegramSender: TelegramSender): SetCommandsSetup {
        return SetCommandsSetup(resource.inputStream, objectMapper, telegramSender)
    }
}