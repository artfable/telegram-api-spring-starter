package com.artfable.telegram.api.starter.services

import com.artfable.telegram.api.Behaviour
import com.artfable.telegram.api.CallbackBehaviour
import com.artfable.telegram.api.Update
import com.artfable.telegram.api.WebhookTelegramBot
import com.artfable.telegram.api.service.TelegramSender
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.Resource
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import javax.annotation.PostConstruct
import javax.annotation.PreDestroy

/**
 * @author aveselov
 * @since 10/02/2021
 */
@RestController
abstract class WebhookTelegramBotSpringWrapper(
    private val url: String,
    private val cert: Resource? = null,
    private val behaviours: Set<Behaviour>,
    private val callbackBehaviours: Set<CallbackBehaviour> = setOf(),
    private val skipFailed: Boolean = true
) {

    @Autowired
    private lateinit var telegramSender: TelegramSender

    private lateinit var webhookTelegramBot: WebhookTelegramBot

    /**
     * For compatibility with java
     */
    constructor(url: String, behaviours: Set<Behaviour>, callbackBehaviours: Set<CallbackBehaviour> = setOf()) :
            this(url, null, behaviours, callbackBehaviours, true)

    /**
     * For compatibility with java
     */
    constructor(
        url: String,
        cert: Resource? = null,
        behaviours: Set<Behaviour>,
        callbackBehaviours: Set<CallbackBehaviour> = setOf()
    ) :
            this(url, cert, behaviours, callbackBehaviours, true)

    @PostConstruct
    private fun init() {
        webhookTelegramBot =
            WebhookTelegramBot(telegramSender, url, cert?.inputStream, behaviours, callbackBehaviours, skipFailed)
        webhookTelegramBot.setWebhook()
    }

    @PreDestroy
    private fun destroy() {
        webhookTelegramBot.removeWebhook()
    }

    @PostMapping
    private fun getUpdate(@RequestBody update: Update): ResponseEntity<Any?> {
        webhookTelegramBot.getUpdate(update)
        return ResponseEntity.ok().build()
    }

}