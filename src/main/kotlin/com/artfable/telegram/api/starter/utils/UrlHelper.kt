package com.artfable.telegram.api.starter.utils

import org.springframework.web.util.UriComponentsBuilder
import java.net.URI

fun getUri(url: String, urlParams: Map<String, String?>, queryParams: Map<String, Any?>? = null): URI {
    val builder = UriComponentsBuilder.fromHttpUrl(url)

    queryParams?.forEach { (name: String, values: Any?) ->
        builder.queryParam(name, values)
    }
    return builder.buildAndExpand(urlParams).encode().toUri()
}